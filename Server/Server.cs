﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    /// <summary>
    /// Класс описывающий реализацию сервера
    /// </summary>
    class Server
    {
        private List<Thread> allThreads;
        private List<Socket> clients;
        private Socket socket;
        private IPEndPoint endPoint;
        private TextBox log;
        private Thread prizeThread;
        private Thread windThread;

        private bool windLeft;
        private bool windRight;

        private byte prizeTimer;
        private byte windTimer;
        private int prizeType;

        public Server(string ip, int port, TextBox log)
        {
            clients = new List<Socket>();
            allThreads = new List<Thread>();
            socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            endPoint = new IPEndPoint(IPAddress.Parse(ip), port);
            this.windLeft = false;
            this.windRight = false;
            prizeType = 0;
            this.log = log;
            prizeTimer = 15;
            windTimer = 30;
        }

        /// <summary>
        /// Метод в котором выполняется инициализация сервера и потоков
        /// </summary>
        public void StartServer()
        {
            try
            {
                socket.Bind(endPoint);
                socket.Listen(2);
                socket.BeginAccept(AcceptCallback, null);
                prizeThread = new Thread(PrizeHandler);
                windThread = new Thread(WindHandler);
                prizeThread.Start();
                windThread.Start();
                allThreads.Add(prizeThread);
                allThreads.Add(windThread);
                log.Text += "Сервер запущен\r\n";

            }
            catch(SocketException ex)
            {
                log.Text += ex + "\r\n";
                socket.Close();
            }
        }

        /// <summary>
        /// Метод в котором выполняется закрытие открытых сетевых сокетов и потоков
        /// </summary>
        public void CloseServer()
        {
            try
            {
                foreach(var thread in allThreads)
                {
                    thread.Abort();
                }

                foreach (var client in clients)
                {
                    client.Close();
                }
                socket.Close();
                log.Text += "\r\nСервер выключен\r\n";
            }
            catch (SocketException ex)
            {
                log.Text += ex + "\r\n";      
            }
        }

        /// <summary>
        /// Метод в котором выполняется прием соединений
        /// </summary>
        /// <param name="ar"></param>
        private void AcceptCallback(IAsyncResult ar)
        {
            try
            {
                Socket client = socket.EndAccept(ar);
                Thread thread = new Thread(HandleClient);
                thread.Start(client);
                allThreads.Add(thread);
                clients.Add(client);
                socket.BeginAccept(AcceptCallback, null);
            }
            catch(Exception ex)
            {
                foreach (var thread in allThreads)
                {
                    thread.Abort();
                }

                foreach (var client in clients)
                {
                    client.Close();
                }
                socket.Close();
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary>
        /// Метод в котором выполняется прием данных от клиентов и отправка в выделеном потоке
        /// </summary>
        /// <param name="o"></param>
        private void HandleClient(object o)
        {
            Socket client = (Socket)o;
            MemoryStream msRead = new MemoryStream(new byte[256], 0, 256, true, true);
            MemoryStream msWrite = new MemoryStream(new byte[256], 0, 256, true, true);
            BinaryReader reader = new BinaryReader(msRead);
            BinaryWriter writer = new BinaryWriter(msWrite);

            while (true)
            { 
                try
                {
                    client.Receive(msRead.GetBuffer());
                    msRead.Position = 0;
                    msWrite.Position = 0;
                    int x = reader.ReadInt32();
                    int y = reader.ReadInt32();
                    double hp = reader.ReadDouble();
                    double armor = reader.ReadDouble();
                    double fuel = reader.ReadDouble();
                    bool left = reader.ReadBoolean();
                    bool right = reader.ReadBoolean();
                    bool down = reader.ReadBoolean();

                    writer.Write(x);
                    writer.Write(y);

                    writer.Write(hp);
                    writer.Write(armor);
                    writer.Write(fuel);

                    writer.Write(left);
                    writer.Write(right);
                    writer.Write(down);

                    writer.Write(prizeType);
                    writer.Write(windLeft);
                    writer.Write(windRight);

                    if (prizeType > 0)
                    {
                        SendAllClients(msWrite);
                        prizeType = 0;
                    }
                    else if (windLeft)
                    {
                        SendAllClients(msWrite);
                        windLeft = false;
                    }
                    else if (windRight)
                    {
                        SendAllClients(msWrite);
                        windRight = false;
                    }
                    else
                    {
                        SendNotAllClients(client, msWrite);
                    }
                    
                }
                catch(SocketException ex)
                {
                    client.Close();
                    msRead.Close();
                    msWrite.Close();
                    reader.Close();
                    writer.Close();
                    break;
                }
            }
        }

        /// <summary>
        /// Метод в котором выполняется рассылка данных всем клиентам
        /// </summary>
        /// <param name="msWrite"></param>
        private void SendAllClients(MemoryStream msWrite)
        {
            try
            { 
                foreach (var c in clients)
                {
                    c.Send(msWrite.GetBuffer());
                }
            }
            catch (Exception ex)
            {
                foreach (var c in clients)
                {
                    c.Close();
                }
                msWrite.Close();
                socket.Close();
            }
}
        /// <summary>
        /// Метод в котором выполняется рассылка данных конкретному клиенту
        /// </summary>
        /// <param name="client"></param>
        /// <param name="msWrite"></param>
        private void SendNotAllClients(Socket client, MemoryStream msWrite)
        {
            try
            {
                foreach (var c in clients)
                {
                    if (c != client)
                    {
                        c.Send(msWrite.GetBuffer());
                    }
                }
            }
            catch (Exception ex)
            {
                client.Close();
                socket.Close();
            }
        }

        /// <summary>
        /// Метод в котором генерация номера приза
        /// </summary>
        private void PrizeHandler()
        {  
            Random random = new Random();
            while (true)
            {
                if (clients.Count > 1)
                {               
                    prizeTimer--;
                    Thread.Sleep(1000);
                    if (prizeTimer == 0)
                    {
                        prizeType = random.Next(1, 7);
                        prizeTimer = 15;
                    }
                }
            }
        }

        /// <summary>
        /// Метод в котором генерация ветра и его направление
        /// </summary>
        private void WindHandler()
        {
            Random random = new Random();
            while (true)
            {
                if (clients.Count > 1)
                {
                    windTimer--;
                    Thread.Sleep(1000);
                    if (windTimer == 0)
                    {
                        int digit = random.Next(1, 3);
                        if (digit == 1)
                            windLeft = true;
                        if (digit == 2)
                            windRight = true;
                        windTimer = 30;
                    }
                }
            }
        }
    }
}
