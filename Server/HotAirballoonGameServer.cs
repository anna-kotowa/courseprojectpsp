﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    public partial class HotAirballoonGameServer : Form
    {
        private bool serverStatus = false;
        private Server server;

        public HotAirballoonGameServer()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(!serverStatus)
            {
                server = new Server(textBox1.Text, Convert.ToInt32(textBox2.Text), textBox3);
                server.StartServer();
                serverStatus = true;
                button1.Text = "Отключить";
                textBox1.Enabled = false;
                textBox2.Enabled = false;
            }
            else
            {
                if(server != null)
                {
                    server.CloseServer();
                    serverStatus = false;
                    button1.Text = "Включить";
                    textBox1.Enabled = true;
                    textBox2.Enabled = true;
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            server.CloseServer();
        }
    }
}
