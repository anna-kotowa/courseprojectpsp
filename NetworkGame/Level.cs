﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkGame
{
    /// <summary>
    /// Класс описывающий уровень(спрайты) игры
    /// </summary>
    public class Level
    {
        private Vector2 position;
        private Vector2 size;
        private Texture2D levelSprite;

        public RectangleF ColRec
        {
            get
            {
                return new RectangleF(position.X - size.X / 2f, position.Y - size.Y / 2f, size.X, size.Y);
            }
        }
        public RectangleF DrawRec
        {
            get
            {
                RectangleF colRec = ColRec;
                colRec.X = colRec.X - 5;
                colRec.Width = colRec.Width + 10;
                return colRec;
            }
        }

        public Texture2D LevelSprite { get => levelSprite; set => levelSprite = value; }

        public Level(Vector2 startPos)
        {
            this.position = startPos;
            this.size = new Vector2(80, 80);
        }
        public void Draw()
        {
            RectangleF rec = DrawRec;
            rec.X += rec.Width;
            rec.Width = -rec.Width;
            Spritebatch.DrawSprite(levelSprite, rec);
        }
    }
}
