﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkGame
{
    /// <summary>
    /// Класс описывающий сетевое взаимодействие
    /// </summary>
    public class Network
    {
        /// <summary>
        /// Сокет для подключения к серверу
        /// </summary>
        private Socket socket;
        private IPEndPoint endPoint;

        /// <summary>
        /// Объект локального игрока
        /// </summary>
        private Player localPlayer;

        /// <summary>
        /// Объект стороннего игрока
        /// </summary>
        private Player remotePlayer;

        /// <summary>
        /// Объект фабрики здоровья
        /// </summary>
        private PrizeFabric hpPrizeFabric;

        /// <summary>
        /// Объект фабрики брони
        /// </summary>
        private PrizeFabric armorPrizeFabric;

        /// <summary>
        /// Объект фабрики топлива
        /// </summary>
        private PrizeFabric fuelPrizeFabric;

        /// <summary>
        /// Объект фабрики урона
        /// </summary>
        private PrizeFabric damagePrizeFabric;

        /// <summary>
        /// Объект фабрики скорости
        /// </summary>
        private PrizeFabric speedPrizeFabric;

        private bool isGameOver;

        /// <summary>
        /// Коллекция призов
        /// </summary>
        private List<Prize> prizes;

        private Random random;

        private MemoryStream ms;
        private BinaryReader reader;
        private BinaryWriter writer;

        public Player LocalPlayer { get => localPlayer; set => localPlayer = value; }
        public Player RemotePlayer { get => remotePlayer; set => remotePlayer = value; }
        public List<Prize> Prizes { get => prizes; set => prizes = value; }

        public bool IsGameOver { get => isGameOver; }

        /// <summary>
        /// Конструктор принимающий параметры ip-адреса и порта
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        public Network(string ip, int port)
        {
            hpPrizeFabric = new HPPrizeFabric();
            armorPrizeFabric = new ArmorPrizeFabric();
            fuelPrizeFabric = new FuelPrizeFabric();
            damagePrizeFabric = new DamagePrizeFabric();
            speedPrizeFabric = new SpeedPrizeFabric();

            endPoint = new IPEndPoint(IPAddress.Parse(ip), port);
            socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            random = new Random();
            ms = new MemoryStream(new byte[256], 0, 256, true, true);
            reader = new BinaryReader(ms);
            writer = new BinaryWriter(ms);
            prizes = new List<Prize>();
            isGameOver = false;
        }

        /// <summary>
        /// Метод проверки на условия окончания игры
        /// </summary>
        public void EndGame()
        {
            if (localPlayer != null && localPlayer.HP <= 0)
            {
                MessageBox.Show("Вы проиграли !");
                localPlayer = null;
                isGameOver = true;

            }
            if (remotePlayer != null && remotePlayer.HP <= 0)
            {
                MessageBox.Show("Вы победили !");
                remotePlayer = null;
                isGameOver = true;
            }
        }

        /// <summary>
        /// Метод инициализации соединения
        /// </summary>
        public void StartConnection()
        {  
            try
            {
                socket.Connect(endPoint);
            }
            catch (SocketException ex)
            {
                CloseConnection(ex.ToString());
            }
        }

        /// <summary>
        /// Метод асинхронной передачи данных на сервер
        /// </summary>
        public void SendRecieve()
        {
            try
            {
                Thread.Sleep(2);
                if (localPlayer != null)
                {
                    ms.Position = 0;
                    int x = Convert.ToInt32(localPlayer.Position.X);
                    int y = Convert.ToInt32(localPlayer.Position.Y);
                    double hp = localPlayer.HP;
                    double armor = localPlayer.Armor;
                    double fuel = localPlayer.Fuel;
                    bool left = localPlayer.Left;
                    bool right = localPlayer.Right;
                    bool down = localPlayer.Down;

                    writer.Write(x);
                    writer.Write(y);
                    writer.Write(hp);
                    writer.Write(armor);
                    writer.Write(fuel);
                    writer.Write(left);
                    writer.Write(right);
                    writer.Write(down);

                    socket.BeginSend(ms.GetBuffer(), 0, (int)ms.Length, SocketFlags.None, new AsyncCallback(SendCallBack), null);
                }
                else
                {
                    int x = random.Next(-450, 450);
                    int y = 80;
                    Vector2 startPos = new Vector2(x, y);
                    localPlayer = new Player(startPos, true);
                }
            }
            catch(Exception ex)
            {
                CloseConnection(ex.ToString());
            }
        }

        private void SendCallBack(IAsyncResult ar)
        {
            try
            {
                socket.EndSend(ar);
                socket.BeginReceive(ms.GetBuffer(), 0, (int)ms.Length, SocketFlags.None, new AsyncCallback(RecieveCallBack), null);
            }
            catch(Exception ex)
            {
                CloseConnection(ex.ToString());
            }
        }

        /// <summary>
        /// Метод асинхронного приема данных с сервера
        /// </summary>
        /// <param name="ar"></param>
        private void RecieveCallBack(IAsyncResult ar)
        {
            try
            {
                socket.EndReceive(ar);
                ms.Position = 0;
                int x = reader.ReadInt32();
                int y = reader.ReadInt32();
                double hp = reader.ReadDouble();
                double armor = reader.ReadDouble();
                double fuel = reader.ReadDouble();
                bool left = reader.ReadBoolean();
                bool right = reader.ReadBoolean();
                bool down = reader.ReadBoolean();
                int prizeType = reader.ReadInt32();
                bool windLeft = reader.ReadBoolean();
                bool windRight = reader.ReadBoolean();
                
                Vector2 newPos = new Vector2(x, y);

                if (prizeType > 0)
                {
                    SpawnPrize(prizeType);
                }
                else if(windLeft || windRight)
                {
                    localPlayer.WindLeft = windLeft;
                    localPlayer.WindRight = windRight;
                    remotePlayer.WindLeft = windLeft;
                    remotePlayer.WindRight = windRight;
                }
                else
                {
                    if (remotePlayer == null)
                        remotePlayer = new Player(newPos, false);
                    else
                    {
                        remotePlayer.Position = newPos;
                        remotePlayer.HP = hp;
                        remotePlayer.Armor = armor;
                        remotePlayer.Fuel = fuel;
                        remotePlayer.Left = left;
                        remotePlayer.Right = right;
                        remotePlayer.Down = down;
                    }
                }
            }
            catch(Exception ex)
            {
                CloseConnection(ex.ToString());
            }
        }

        /// <summary>
        /// Метод добавления приза на экран
        /// </summary>
        /// <param name="prizeType"></param>
        private void SpawnPrize(int prizeType)
        {
            Vector2 spawnPoint = new Vector2(-500, 0);
            Vector2 size = new Vector2(30, 40);
            switch (prizeType)
            {
                case 1:
                    prizes.Add(hpPrizeFabric.CreatePrize(spawnPoint, size, localPlayer, remotePlayer));
                    break;
                case 2:
                    prizes.Add(armorPrizeFabric.CreatePrize(spawnPoint, size, localPlayer, remotePlayer));
                    break;
                case 3:
                    prizes.Add(fuelPrizeFabric.CreatePrize(spawnPoint, size, localPlayer, remotePlayer));
                    break;
                case 4:
                    prizes.Add(damagePrizeFabric.CreatePrize(spawnPoint, new Vector2(120, 40), localPlayer, remotePlayer));
                    break;
                case 5:
                    prizes.Add(speedPrizeFabric.CreatePrize(spawnPoint, new Vector2(120, 40), localPlayer, remotePlayer));
                    break;
                default:
                    break;
            }
        }

        public void CloseConnection(string message = "Соединение закрыто")
        {
            try
            {
                if (socket != null)
                {
                    socket.Disconnect(false);
                    socket.Close();
                    socket = null;
                    ms.Close();
                    writer.Close();
                    reader.Close();
                    isGameOver = true;
                    MessageBox.Show(message);
                }
            }
            catch {
                MessageBox.Show(message);
            }
        }
    }
}
