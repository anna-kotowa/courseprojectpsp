﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkGame.Weapons
{
    /// <summary>
    /// Класс описывающий снаряд ракеты
    /// </summary>
    public class Missle : Weapon
    {
        /// <summary>
        /// Переменная которая хранит значение направление спрайта снаряда
        /// </summary>
        private bool direct;

        /// <summary>
        /// Переменная которая хранит значение урон снаряда
        /// </summary>
        private int damage;

        /// <summary>
        /// Переменная которая хранит значение скорость снаряда
        /// </summary>
        private float speed;

        /// <summary>
        /// Конструктор снаряда с параметрами начальной позиции, размера, спрайта, значения принадлежности снаряда, направления, урона и скорости
        /// </summary>
        public Missle(Vector2 startPos, Vector2 size, Texture2D sprite, bool fromLocal, bool direct, int damage, float speed)
            : base(startPos, size, sprite, fromLocal)
        {
            this.direct = direct;
            Sprite = sprite;
            this.damage = damage;
            this.speed = speed;
        }

        /// <summary>
        /// Свойство для доступа к переменной урона снаряда
        /// </summary>
        public int Damage { get => damage; set => damage = value; }

        /// <summary>
        /// Свойство для доступа к переменной скорости снаряда
        /// </summary>
        public float Speed { get => speed; set => speed = value; }

        /// <summary>
        /// Свойство для доступа к переменной направления снаряда
        /// </summary>
        public bool Direct { get => direct; set => direct = value; }

        /// <summary>
        /// Метод который вычисляет положение ракеты на экране
        /// </summary>
        public override void Update()
        {
            if(!Hit)
            {
                if (!direct)
                    Position += new Vector2(Speed, 0);
                else
                    Position += new Vector2(-Speed, 0);
                Lifetime -= 0.1f;
                ResolveCollision();
            }
        }

        /// <summary>
        /// Метод который вычисляет коллизии ракеты с игроком
        /// </summary>
        public override void ResolveCollision()
        {
            if (RemotePlayerRect != null && RemotePlayer != null)
            {
                if (RemotePlayerRect.IntersectsWith(ColRec))
                {
                    RemotePlayer.HP -= damage;
                    Hit = true;
                }
            }
        }
    }
}
