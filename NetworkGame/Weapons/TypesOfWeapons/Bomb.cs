﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkGame.Weapons
{
    /// <summary>
    /// Класс описывающий снаряд бомбы
    /// </summary>
    public class Bomb : Weapon
    {
        /// <summary>
        /// Переменная которая хранит значение урон снаряда
        /// </summary>
        private int damage;

        /// <summary>
        /// Переменная которая хранит значение скорость снаряда
        /// </summary>
        private float speed;

        /// <summary>
        /// Конструктор снаряда с параметрами начальной позиции, размера, спрайта, значения принадлежности снаряда, урона и скорости
        /// </summary>
        public Bomb(Vector2 startPos, Vector2 size, Texture2D sprite, bool fromLocal,int damage, float speed)
            : base(startPos, size, sprite, fromLocal)
        {
            Sprite = sprite;
            this.damage = damage;
            this.speed = speed;
        }

        /// <summary>
        /// Свойство для доступа к переменной урона снаряда
        /// </summary>
        public int Damage { get => damage; set => damage = value; }

        /// <summary>
        /// Свойство для доступа к переменной скорости снаряда
        /// </summary>
        public float Speed { get => speed; set => speed = value; }

        /// <summary>
        /// Метод который вычисляет положение бомбы на экране
        /// </summary>
        public override void Update()
        {
            if (!Hit)
            {
                Position += new Vector2(0, Speed);
                Lifetime -= 0.1f;
                ResolveCollision();
            }
        }

        /// <summary>
        /// Метод который вычисляет коллизии бомбы с игроком
        /// </summary>
        public override void ResolveCollision()
        {
            if (RemotePlayerRect != null && RemotePlayer != null)
            {
                if (RemotePlayerRect.IntersectsWith(ColRec))
                {
                    RemotePlayer.HP -= damage;
                    Hit = true;
                }
            }
        }
    }
}
