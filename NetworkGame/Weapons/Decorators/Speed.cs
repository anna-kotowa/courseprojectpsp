﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkGame.Weapons
{
    /// <summary>
    /// Класс описывающий декоратор скорости
    /// </summary>
    public class Speed : WeaponDecorator
    {
        /// <summary>
        /// Объект декорируемого оружия
        /// </summary>
        private Weapon weapon;

        /// <summary>
        /// Константа которая содержит коэффициент повышения скорости снаряда ракеты
        /// </summary>
        private const float addMissleSpeed = 5f;

        /// <summary>
        /// Константа которая содержит коэффициент повышения скорости снаряда бомбы
        /// </summary>
        private const float addBombSpeed = 3f;

        /// <summary>
        /// Конструктор декоратора скорости с параметрами начальной позиции, размера, спрайта и значения принадлежности снаряда и объекта декорируемого оружия
        /// </summary>
        public Speed(Vector2 startPos, Vector2 size, Texture2D sprite, bool fromLocal, Weapon weapon)
            : base(startPos, size, sprite, fromLocal, weapon)
        {
            this.weapon = weapon;
        }

        /// <summary>
        /// Метод который описывает логику повышения скорости снаряда
        /// </summary>
        public override Weapon Modify()
        {
            if (weapon is Missle)
            {
                Missle missle = (Missle)weapon;
                missle.Speed = missle.Speed + addMissleSpeed;
                return missle;
            }
            else
            {
                Bomb bomb = (Bomb)weapon;
                bomb.Speed = bomb.Speed + addBombSpeed;
                return bomb;
            }

        }

        public override void Update()
        {
            throw new NotImplementedException();
        }

        public override void ResolveCollision()
        {
            throw new NotImplementedException();
        }
    }
}
