﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkGame.Weapons
{
    /// <summary>
    /// Класс описывающий декоратор урона
    /// </summary>
    public class Damage: WeaponDecorator
    {
        /// <summary>
        /// Объект декорируемого оружия
        /// </summary>
        private Weapon weapon;

        /// <summary>
        /// Константа которая содержит коэффициент повышения урона снаряда ракеты
        /// </summary>
        private const int addMissleDamage = 5;

        /// <summary>
        /// Константа которая содержит коэффициент повышения урона снаряда бомбы
        /// </summary>
        private const int addBombDamage = 10;

        /// <summary>
        /// Конструктор декоратора урона с параметрами начальной позиции, размера, спрайта и значения принадлежности снаряда и объекта декорируемого оружия
        /// </summary>
        public Damage(Vector2 startPos, Vector2 size, Texture2D sprite, bool fromLocal, Weapon weapon)
            : base(startPos, size, sprite, fromLocal, weapon)
        {
            this.weapon = weapon;
        }

        /// <summary>
        /// Метод который описывает логику повышения урона снаряда
        /// </summary>
        public override Weapon Modify()
        {
            if(weapon is Missle)
            {
                Missle missle = (Missle)weapon;
                missle.Damage = missle.Damage + addMissleDamage;
                return missle;
            }
            else
            {
                Bomb bomb = (Bomb)weapon;
                bomb.Damage = bomb.Damage + addBombDamage;
                return bomb;
            }
           
        }

        public override void Update()
        {
            throw new NotImplementedException();
        }

        public override void ResolveCollision()
        {
            throw new NotImplementedException();
        }
    }
}
