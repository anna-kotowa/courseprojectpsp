﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkGame.Weapons
{
    /// <summary>
    /// Абстрактный класс декоратора оружия(снарядов)
    /// </summary>
    public abstract class WeaponDecorator: Weapon
    {
        /// <summary>
        /// Объект декорируемого оружия
        /// </summary>
        private Weapon weapon;

        /// <summary>
        /// Конструктор декоратора с параметрами начальной позиции, размера, спрайта и значения принадлежности снаряда и объекта декорируемого оружия
        /// </summary>
        public WeaponDecorator(Vector2 startPos, Vector2 size, Texture2D sprite, bool fromLocal, Weapon weapon)
            : base(startPos, size, sprite, fromLocal)
        {
            this.weapon = weapon;
        }

        /// <summary>
        /// Абстрактный метод который описывает логику модификации снаряда
        /// </summary>
        public abstract Weapon Modify();
    }
}
