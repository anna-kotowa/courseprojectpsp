﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkGame.Weapons
{
    /// <summary>
    /// Абстрактный класс представляющий вид снаряда оружия
    /// </summary>
    public abstract class Weapon
    {
        /// <summary>
        /// Позиция снаряда на экране по осям Х и Y
        /// </summary>
        private Vector2 position;

        /// <summary>
        /// Размер спрайта снаряда
        /// </summary>
        private Vector2 size;

        /// <summary>
        /// Ссылка на структуру, которая описывает невидимый прямоугольник стороннего игрока
        /// </summary>
        private RectangleF remotePlayerRect;

        /// <summary>
        /// Ссылка на объект, которая описывает стороннего игрока
        /// </summary>
        private Player remotePlayer;

        /// <summary>
        /// Спрайт приза
        /// </summary>
        private Texture2D sprite;

        /// <summary>
        /// Булева переменная для определения принадлежности снаряда, от локального игрока или стороннего
        /// </summary>
        private bool fromLocal;

        /// <summary>
        /// Булева переменная для определения коллизии снаряда с игроком
        /// </summary>
        private bool hit;

        /// <summary>
        /// Поле с кол-вом ед. времени, которые определяют время жизни снаряда
        /// </summary>
        private double lifetime;

        /// <summary>
        /// Конструктор снаряда с параметрами начальной позиции, размера, спрайта и значения принадлежности снаряда
        /// </summary>
        public Weapon(Vector2 startPos, Vector2 size, Texture2D sprite, bool fromLocal)
        {
            this.position = startPos;
            this.size = size;
            this.fromLocal = fromLocal;
            this.hit = false;
            this.sprite = sprite;
            lifetime = 20;
        }

        /// <summary>
        /// Свойсто для вычисления позиции невидимого прямоугольника снаряда
        /// </summary>
        public RectangleF ColRec
        {
            get
            {
                return new RectangleF(position.X - size.X / 2f, position.Y - size.Y / 2f, size.X, size.Y);
            }
        }

        /// <summary>
        /// Свойство для отрисовки снаряда
        /// </summary>
        public RectangleF DrawRec
        {
            get
            {
                RectangleF colRec = ColRec;
                colRec.X = colRec.X - 5;
                colRec.Width = colRec.Width + 10;
                return colRec;
            }
        }

        /// <summary>
        /// Свойство для доступа к структуре невидимого прямоугольника стороннего игрока
        /// </summary>
        public RectangleF RemotePlayerRect { get => remotePlayerRect; set => remotePlayerRect = value; }

        /// <summary>
        /// Свойство для доступа к объекту стороннего игрока
        /// </summary>
        public Player RemotePlayer { get => remotePlayer; set => remotePlayer = value; }

        /// <summary>
        /// Свойство для доступа к переменной спрайта
        /// </summary>
        public Texture2D Sprite { get => sprite; set => sprite = value; }

        /// <summary>
        /// Свойство для доступа к объекту позиции снаряда
        /// </summary>
        public Vector2 Position { get => position; set => position = value; }

        /// <summary>
        /// Свойство для доступа к объекту размера снаряда
        /// </summary>
        public Vector2 Size { get => size; set => size = value; }

        /// <summary>
        /// Свойство для доступа к переменной принадлежности снаряда
        /// </summary>
        public bool FromLocal { get => fromLocal; set => fromLocal = value; }

        /// <summary>
        /// Свойство для доступа к переменной определения попадания снаряда
        /// </summary>
        public bool Hit { get => hit; set => hit = value; }

        /// <summary>
        /// Свойство для доступа к переменной времени жизни снаряда
        /// </summary>
        public double Lifetime { get => lifetime; set => lifetime = value; }

        /// <summary>
        /// Абстрактный метод который описывает логику работы снаряда, реализующего его
        /// </summary>
        public abstract void Update();

        /// <summary>
        /// Абстрактный метод который описывает коллизию снаряда с игроком
        /// </summary>
        public abstract void ResolveCollision();

        /// <summary>
        /// Метод для отрисовки снаряда на экране
        /// </summary>
        public void Draw(Texture2D sprite)
        {
            RectangleF rec = DrawRec;
            rec.X += rec.Width;
            rec.Width = -rec.Width;
            Spritebatch.DrawSprite(sprite, rec);
        }
    }
}
