﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using System.Drawing;

namespace NetworkGame
{
    /// <summary>
    /// Абстрактный класс "Приза", который представляет производимый "продукт" на "фабрике"
    /// </summary>
    public abstract class Prize
    {
        /// <summary>
        /// Позиция приза на экране по осям Х и Y
        /// </summary>
        private Vector2 position;

        /// <summary>
        /// Размер спрайта приза
        /// </summary>
        private Vector2 size;

        /// <summary>
        /// Ссылка на структуру, которая описывает невидимый прямоугольник локального игрока
        /// </summary>
        private RectangleF localPlayerRect;

        /// <summary>
        /// Ссылка на структуру, которая описывает невидимый прямоугольник стороннего игрока
        /// </summary>
        private RectangleF remotePlayerRect;

        /// <summary>
        /// Ссылка на объект, которая описывает локального игрока
        /// </summary>
        private Player localPlayer;

        /// <summary>
        /// Ссылка на объект, которая описывает стороннего игрока
        /// </summary>
        private Player remotePlayer;

        /// <summary>
        /// Конструктор приза с параметрами начальной позиции, размера, локального и стороннего игрока
        /// </summary>
        public Prize(Vector2 startPos, Vector2 size, Player localPlayer, Player remotePlayer)
        {
            this.position = startPos;
            this.size = size;
            if (localPlayer != null && remotePlayer != null)
            {
                this.localPlayerRect = localPlayer.ColRec;
                this.remotePlayerRect = remotePlayer.ColRec;
                this.localPlayer = localPlayer;
                this.remotePlayer = remotePlayer;
            }
        }

        /// <summary>
        /// Абстрактный метод которая описывает логику работы приза, реализующего его
        /// </summary>
        public abstract void Update();

        /// <summary>
        /// Абстрактный метод которая описывает логику коллизий приза и игрока
        /// </summary>
        public abstract void ResolveCollision();

        /// <summary>
        /// Абстрактный метод которая описывает отрисовку приза на экране
        /// </summary>
        public abstract void Draw();

    }
}
