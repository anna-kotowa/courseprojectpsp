﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using System.Drawing;

namespace NetworkGame
{
    /// <summary>
    /// Абстрактный класс фабрики призов
    /// </summary>
    public abstract class PrizeFabric
    {
        public PrizeFabric()
        { }

        /// <summary>
        /// Абстрактный метод, для создания призов с параметрами начальной позиции, размера, локального и стороннего игрока
        /// </summary>
        abstract public Prize CreatePrize(Vector2 position, Vector2 size, Player localPlayer, Player remotePlayer);
    }
}
