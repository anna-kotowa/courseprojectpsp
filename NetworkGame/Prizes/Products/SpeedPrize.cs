﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using System.Drawing;
using NetworkGame.Weapons;

namespace NetworkGame
{
    /// <summary>
    /// Класс приза со скоростью
    /// </summary>
    public class SpeedPrize : Prize
    {
        /// <summary>
        /// Спрайт приза
        /// </summary>
        private Texture2D prizeSprite;
        /// <summary>
        /// Позиция приза на экране по осям Х и Y
        /// </summary>
        private Vector2 position;

        /// <summary>
        /// Размер спрайта приза
        /// </summary>
        private Vector2 size;

        /// <summary>
        /// Ссылка на структуру, которая описывает невидимый прямоугольник локального игрока
        /// </summary>
        private RectangleF localPlayerRect;

        /// <summary>
        /// Ссылка на структуру, которая описывает невидимый прямоугольник стороннего игрока
        /// </summary>
        private RectangleF remotePlayerRect;

        /// <summary>
        /// Ссылка на объект, которая описывает локального игрока
        /// </summary>
        private Player localPlayer;

        /// <summary>
        /// Ссылка на объект, которая описывает стороннего игрока
        /// </summary>
        private Player remotePlayer;

        /// <summary>
        /// Булева переменная для определения коллизии приза с игроком
        /// </summary>
        private bool isHit;

        /// <summary>
        /// Поле с кол-вом ед. времени, которые определяют время жизни приза
        /// </summary>
        private double lifeTime;

        /// <summary>
        /// Конструктор приза скорости с параметрами начальной позиции, размера, локального и стороннего игрока
        /// </summary>
        public SpeedPrize(Vector2 startPos, Vector2 size, Player localPlayer, Player remotePlayer, bool isTest = false)
               : base(startPos, size, localPlayer, remotePlayer)
        {
            this.position = startPos;
            this.size = size;
            if (localPlayer != null && remotePlayer != null)
            {
                this.localPlayerRect = localPlayer.ColRec;
                this.remotePlayerRect = remotePlayer.ColRec;
                this.localPlayer = localPlayer;
                this.remotePlayer = remotePlayer;
            }
            isHit = false;
            lifeTime = 100;
        }

        /// <summary>
        /// Свойсто для вычисления позиции невидимого прямоугольника приза
        /// </summary>
        public RectangleF ColRec
        {
            get
            {
                return new RectangleF(position.X - size.X / 2f, position.Y - size.Y / 2f, size.X, size.Y);
            }
        }

        /// <summary>
        /// Свойство для отрисовки приза
        /// </summary>
        public RectangleF DrawRec
        {
            get
            {
                RectangleF colRec = ColRec;
                colRec.X = colRec.X - 5;
                colRec.Width = colRec.Width + 10;
                return colRec;
            }
        }

        /// <summary>
        /// Свойство для доступа к переменной спрайта
        /// </summary>
        public Texture2D PrizeSprite { get => prizeSprite; set => prizeSprite = value; }

        public Vector2 Position { get => position; set => position = value; }

        /// <summary>
        /// Свойство для доступа к переменной определения коллизии
        /// </summary>
        public bool IsHit { get => isHit; }

        /// <summary>
        /// Метод который вычисляет позицию приза на экране
        /// </summary>
        public override void Update()
        {
            if (!isHit)
            {
                position += new Vector2(4f, 0f);
                lifeTime -= 1;
                ResolveCollision();
            }
        }

        /// <summary>
        /// Метод который вычисляет коллизию приза с игроком
        /// </summary>
        public override void ResolveCollision()
        {
            if (localPlayerRect != null && localPlayer != null &&
                remotePlayerRect != null && remotePlayer != null)
            {
                if (localPlayerRect.IntersectsWith(ColRec) && !isHit)
                {
                    ModifyWeapon(true);
                    isHit = true;
                }
                if (remotePlayerRect.IntersectsWith(ColRec) && !isHit)
                {
                    ModifyWeapon(false);
                    isHit = true;
                }
            }
        }

        /// <summary>
        /// Метод для модификации оружия
        /// </summary>
        private void ModifyWeapon(bool local)
        {
            if (local)
            {
                try
                {
                    for (int i = 0; i < localPlayer.Missles.Count; i++)
                    {
                        localPlayer.Missles[i] = (new Speed(localPlayer.Missles[i].Position,
                                                            localPlayer.Missles[i].Size,
                                                            localPlayer.Missles[i].Sprite,
                                                            localPlayer.Missles[i].FromLocal,
                                                            localPlayer.Missles[i])).Modify();
                    }
                    for (int i = 0; i < localPlayer.Bombs.Count; i++)
                    {
                        localPlayer.Bombs[i] = (new Speed(localPlayer.Bombs[i].Position,
                                                          localPlayer.Bombs[i].Size,
                                                          localPlayer.Bombs[i].Sprite,
                                                          localPlayer.Bombs[i].FromLocal,
                                                          localPlayer.Bombs[i])).Modify();
                    }
                }
                catch { }
            }
            else
            {
                try
                {
                    for (int i = 0; i < remotePlayer.Missles.Count; i++)
                    {
                        remotePlayer.Missles[i] = (new Speed(remotePlayer.Missles[i].Position,
                                                             remotePlayer.Missles[i].Size,
                                                             remotePlayer.Missles[i].Sprite,
                                                             remotePlayer.Missles[i].FromLocal,
                                                             remotePlayer.Missles[i])).Modify();
                    }
                    for (int i = 0; i < remotePlayer.Bombs.Count; i++)
                    {
                        remotePlayer.Bombs[i] = (new Speed(remotePlayer.Bombs[i].Position,
                                                           remotePlayer.Bombs[i].Size,
                                                           remotePlayer.Bombs[i].Sprite,
                                                           remotePlayer.Bombs[i].FromLocal,
                                                           remotePlayer.Bombs[i])).Modify();
                    }
                }
                catch { }
            }
        }

        /// <summary>
        /// Метод для отрисовки приза на экране
        /// </summary>
        public override void Draw()
        {
            RectangleF rec = DrawRec;
            rec.X += rec.Width;
            rec.Width = -rec.Width;
            Spritebatch.DrawSprite(prizeSprite, rec);
        }
    }
}
