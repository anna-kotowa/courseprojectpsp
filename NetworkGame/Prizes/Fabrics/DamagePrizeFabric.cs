﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using System.Drawing;

namespace NetworkGame
{
    /// <summary>
    /// Фабрика призов с повышением урона оружия
    /// </summary>
    public class DamagePrizeFabric : PrizeFabric
    {
        /// <summary>
        /// Метод создания приза с повышением урона оружия
        /// </summary>
        public override Prize CreatePrize(Vector2 position, Vector2 size, Player localPlayer, Player remotePlayer)
        {
            return new DamagePrize(position, size, localPlayer, remotePlayer);
        }
    }
}
