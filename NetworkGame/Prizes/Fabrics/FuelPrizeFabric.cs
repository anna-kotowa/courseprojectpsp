﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using System.Drawing;

namespace NetworkGame
{
    /// <summary>
    /// Фабрика призов с топливом
    /// </summary>
    public class FuelPrizeFabric : PrizeFabric
    {
        /// <summary>
        /// Метод создания приза с топливом
        /// </summary>
        public override Prize CreatePrize(Vector2 position, Vector2 size, Player localPlayer, Player remotePlayer)
        {
            return new FuelPrize(position, size, localPlayer, remotePlayer);
        }
    }
}
