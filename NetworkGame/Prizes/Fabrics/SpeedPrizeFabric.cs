﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using System.Drawing;

namespace NetworkGame
{
    /// <summary>
    /// Фабрика призов с повышением скорости оружия
    /// </summary>
    public class SpeedPrizeFabric : PrizeFabric
    {
        /// <summary>
        /// Метод создания приза с повышением скорости оружия
        /// </summary>
        public override Prize CreatePrize(Vector2 position, Vector2 size, Player localPlayer, Player remotePlayer)
        {
            return new SpeedPrize(position, size, localPlayer, remotePlayer);
        }
    }
}
