﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using System.Drawing;

namespace NetworkGame
{
    /// <summary>
    /// Фабрика призов со здоровьем
    /// </summary>
    public class HPPrizeFabric : PrizeFabric
    {
        /// <summary>
        /// Метод создания приза со здоровьем
        /// </summary>
        public override Prize CreatePrize(Vector2 position, Vector2 size, Player localPlayer, Player remotePlayer)
        {
            return new HPPrize(position, size, localPlayer, remotePlayer);
        }
    }
}
