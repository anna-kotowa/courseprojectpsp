﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Input;
using System.Windows.Forms;
using QuickFont;
using QuickFont.Configuration;
using NetworkGame.Weapons;

namespace NetworkGame
{
    /// <summary>
    /// Класс описывающий главное игровое окно, который наследует от класса GameWindow функционал OpenGL
    /// </summary>
    class Game : GameWindow
    {
        /// <summary>
        /// Константы описывающие значения размеров спрайта уровня
        /// </summary>
        public static int GRIDSIZE = 64, TILESIZE = 128;

        /// <summary>
        /// Ссылка на класс с реализацией сетевого взаимодействия
        /// </summary>
        private Network network;

        /// <summary>
        /// Спрайт блока "земли"
        /// </summary>
        private Texture2D texture;

        /// <summary>
        /// Спрайт воздушного шара
        /// </summary>
        private Texture2D playerTexture;

        /// <summary>
        /// Спрайт приза здоровья
        /// </summary>
        private Texture2D hpTexture;

        /// <summary>
        /// Спрайт приза брони
        /// </summary>
        private Texture2D armorTexture;

        /// <summary>
        /// Спрайт приза топлива
        /// </summary>
        private Texture2D fuelTexture;

        /// <summary>
        /// Спрайт приза урона
        /// </summary>
        private Texture2D damageTexture;

        /// <summary>
        /// Спрайт приза скорости
        /// </summary>
        private Texture2D speedTexture;

        /// <summary>
        /// Спрайт снаряда ракеты повернутого в левую сторону
        /// </summary>
        private Texture2D leftMissleTexture;

        /// <summary>
        /// Спрайт снаряда ракеты повернутого в правую сторону
        /// </summary>
        private Texture2D rightMissleTexture;

        /// <summary>
        /// Спрайт снаряда бомбы
        /// </summary>
        private Texture2D bombTexture;

        /// <summary>
        /// Ссылка на класс с реализацией игровой камеры
        /// </summary>
        private View view;

        /// <summary>
        /// Коллекция невидимых прямоугольников блоков "земли"
        /// </summary>
        private List<RectangleF> levelRects;

        /// <summary>
        /// Коллекция блоков "земли"
        /// </summary>
        private List<Level> levelBlocks;

        public Game(Network network)
            : base(1200, 720, new OpenTK.Graphics.GraphicsMode(32, 24, 0, 8))
        {
            WindowState = WindowState.Maximized;

            levelRects = new List<RectangleF>();
            levelBlocks = new List<Level>();
            GL.Enable(EnableCap.Texture2D);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
            view = new View(Vector2.Zero, 0.7, 0.0);
            Input.Initialize(this);
            this.network = network;
            texture = ContentPipe.LoadTexture("Content/ground.jpg");
            playerTexture = ContentPipe.LoadTexture("Content/balloon.png");
            hpTexture = ContentPipe.LoadTexture("Content/hp.png");
            armorTexture = ContentPipe.LoadTexture("Content/armor.png");
            fuelTexture = ContentPipe.LoadTexture("Content/fuel.png");
            damageTexture = ContentPipe.LoadTexture("damage.png");
            speedTexture = ContentPipe.LoadTexture("speed.png");
            leftMissleTexture = ContentPipe.LoadTexture("LMissle.png");
            rightMissleTexture = ContentPipe.LoadTexture("RMissle.png");
            bombTexture = ContentPipe.LoadTexture("Bomb.png");
            network.StartConnection();
        }

        /// <summary>
        /// Метод который выполняется в момент запуска игрового окна
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            GL.Enable(EnableCap.Texture2D);
        }
        
        /// <summary>
        /// Методо который выполняется при изменении размеров экрана
        /// </summary>
        /// <param name="e"></param>
        protected override void OnResize(EventArgs e)
        {
            GL.Viewport(ClientRectangle);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(-1.0, 1.0, -1.0, 1.0, 0.0, 4.0);
        }
        
        /// <summary>
        /// Метод игрового цикла приложения. Вычисления в методе проводятся раз в 1 кадр
        /// </summary>
        /// <param name="e"></param>
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            if (!network.IsGameOver)
            {
                base.OnUpdateFrame(e);
                network.SendRecieve();
                
                if (network.LocalPlayer != null)
                {
                    network.LocalPlayer.Update();
                }

                if (network.RemotePlayer != null)
                {
                    network.LocalPlayer.UpdateMissles();
                    network.LocalPlayer.UpdateBombs();

                    network.RemotePlayer.Update();
                    network.RemotePlayer.UpdateMissles();
                    network.RemotePlayer.UpdateBombs();

                    network.LocalPlayer.RemotePlayerRect = network.RemotePlayer.ColRec;
                    network.LocalPlayer.RemotePlayer = network.RemotePlayer;

                    network.RemotePlayer.RemotePlayerRect = network.LocalPlayer.ColRec;
                    network.RemotePlayer.RemotePlayer = network.LocalPlayer;

                    if (network.RemotePlayer.PlayerSprite.Width == 0 && network.RemotePlayer.PlayerSprite.Height == 0)
                        network.RemotePlayer.PlayerSprite = playerTexture;
                    UpdatePrizes();
                }
                else
                {
                    network.LocalPlayer.UpdateMissles();
                    network.LocalPlayer.UpdateBombs();
                }

                if (network.LocalPlayer.PlayerSprite.Width == 0 && network.LocalPlayer.PlayerSprite.Height == 0)
                    network.LocalPlayer.PlayerSprite = playerTexture;
                network.EndGame();      
            }
            else
            {
                Close();
            }
        }

        /// <summary>
        /// Метод выполняющий отрисовку всех объектов на экране
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            if (!network.IsGameOver)
            {
                base.OnRenderFrame(e);
                GL.Clear(ClearBufferMask.ColorBufferBit);
                GL.ClearColor(Color.Ivory);
 
                DrawPlayerStatusText();
                network.LocalPlayer.Draw();
                network.LocalPlayer.DrawMissles(leftMissleTexture, rightMissleTexture);
                network.LocalPlayer.DrawBombs(bombTexture);
                if (network.RemotePlayer != null)
                {
                    network.RemotePlayer.Draw();
                    network.RemotePlayer.DrawMissles(leftMissleTexture, rightMissleTexture);
                    network.RemotePlayer.DrawBombs(bombTexture);
                    DrawPrizes();
                }
                view.ApplyTransform();
                DrawMap();
                Spritebatch.Begin(Width, Height);
                GL.End();
            
                SwapBuffers();
            }
        }

        /// <summary>
        /// Метод отрисовки игрового уровня("земли")
        /// </summary>
        private void DrawMap()
        {
            if (!network.IsGameOver)
            {
                int posX = -2000;
                for (int i = 0; i < 128; i++)
                {
                    Level level = new Level(new Vector2(posX, 540));
                    levelBlocks.Add(level);
                    levelRects.Add(levelBlocks[i].ColRec);
                    levelBlocks[i].LevelSprite = texture;
                    levelBlocks[i].Draw();
                    posX += 85;
                }

                DrawLevel();

            
                if (network.LocalPlayer.Rects.Count == 0)
                    network.LocalPlayer.Rects = new List<RectangleF>(levelRects);
                if (network.RemotePlayer != null)
                {
                    if (network.RemotePlayer.Rects.Count == 0)
                        network.RemotePlayer.Rects = new List<RectangleF>(levelRects);
                }
            
                levelRects.Clear();
                levelBlocks.Clear();
            }
        }

        /// <summary>
        /// Метод обработы всех призов имеющихся на экране
        /// </summary>
        private void UpdatePrizes()
        {
            if (network.Prizes.Count > 0)
            {
                try
                {
                    foreach (var prize in network.Prizes)
                    {
                        if (prize is HPPrize)
                        {
                            HPPrize hpPrize = (HPPrize)prize;
                            hpPrize.Update();
                            if(hpPrize.IsHit)
                            {
                                network.Prizes.Remove(hpPrize);
                            }
                        }
                        if (prize is ArmorPrize)
                        {
                            ArmorPrize armorPrize = (ArmorPrize)prize;             
                            armorPrize.Update();
                            if (armorPrize.IsHit)
                            {
                                network.Prizes.Remove(armorPrize);
                            }
                        }
                        if (prize is FuelPrize)
                        {
                            FuelPrize fuelPrize = (FuelPrize)prize; 
                            fuelPrize.Update();
                            if (fuelPrize.IsHit)
                            {
                                network.Prizes.Remove(fuelPrize);
                            }
                        }
                        if (prize is DamagePrize)
                        {
                            DamagePrize damagePrize = (DamagePrize)prize;
                            damagePrize.Update();
                            if (damagePrize.IsHit)
                            {
                                network.Prizes.Remove(damagePrize);
                            }
                        }
                        if (prize is SpeedPrize)
                        {
                            SpeedPrize speedPrize = (SpeedPrize)prize;
                            speedPrize.Update();
                            if (speedPrize.IsHit)
                            {
                                network.Prizes.Remove(speedPrize);
                            }
                        }
                    }
                }
                catch { }
            }
        }

        /// <summary>
        /// Метод отрисовки призов на экране
        /// </summary>
        private void DrawPrizes()
        {
            if(network.Prizes.Count > 0)
            {
                try
                {
                    foreach (var prize in network.Prizes)
                    {
                        if(prize is HPPrize)
                        {
                            HPPrize hpPrize = (HPPrize)prize;
                            hpPrize.PrizeSprite = hpTexture;
                            hpPrize.Draw();
                        }
                        if (prize is ArmorPrize)
                        {
                            ArmorPrize armorPrize = (ArmorPrize)prize;
                            armorPrize.PrizeSprite = armorTexture;
                            armorPrize.Draw();
                        }
                        if (prize is FuelPrize)
                        {
                            FuelPrize fuelPrize = (FuelPrize)prize;
                            fuelPrize.PrizeSprite = fuelTexture;
                            fuelPrize.Draw();
                        }
                        if (prize is DamagePrize)
                        {
                            DamagePrize damagePrize = (DamagePrize)prize;
                            damagePrize.PrizeSprite = damageTexture;
                            damagePrize.Draw();
                        }
                        if (prize is SpeedPrize)
                        {
                            SpeedPrize speedPrize = (SpeedPrize)prize;
                            speedPrize.PrizeSprite = speedTexture;
                            speedPrize.Draw();
                        }
                    }
                }
                catch{ }
            }
        }

        private void DrawLevel()
        {
            if (levelBlocks.Count > 0)
            {
                try
                {
                    foreach (var level in levelBlocks)
                    {
                        level.Draw();
                    }
                }
                catch { }
            }
        }

        /// <summary>
        /// Метод для вывода информации об игроке и враге
        /// </summary>
        private void DrawPlayerStatusText()
        {
            string status = "";
            if (network.RemotePlayer == null)
                status = string.Format("ВЫ [Здоровье: {0} Броня: {1} Топливо: {2}]", network.LocalPlayer.HP, network.LocalPlayer.Armor, Math.Round(network.LocalPlayer.Fuel, 1));
            else
                status = string.Format("ВЫ [Здоровье: {0} Броня: {1} Топливо: {2}] ВРАГ [Здоровье: {3} Броня: {4} Топливо: {5}]", network.LocalPlayer.HP, network.LocalPlayer.Armor, Math.Round(network.LocalPlayer.Fuel, 1), network.RemotePlayer.HP, network.RemotePlayer.Armor, Math.Round(network.RemotePlayer.Fuel, 1));
            this.Title = status;
        }
    }
}
