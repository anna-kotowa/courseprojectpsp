﻿using NetworkGame.Weapons;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetworkGame
{
    public partial class HotAirballoonGame : Form
    {
        private Network network;
        private Game game;

        public HotAirballoonGame()
        {
            InitializeComponent();   
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (network == null)
            {
                network = new Network(textBox1.Text, Convert.ToInt32(textBox2.Text));
                game = new Game(network);
                game.Run();
            }
        }
    }
}
