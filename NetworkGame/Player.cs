﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using System.Drawing;
using NetworkGame.Weapons;

namespace NetworkGame
{
    /// <summary>
    /// Класс описывающий игрока(воздушного шара)
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Коллекция ракет
        /// </summary>
        private List<Weapon> missles;

        /// <summary>
        /// Коллекция бомб
        /// </summary>
        private List<Weapon> bombs;
        private Vector2 missleSize;
        private Vector2 bombSize;

        /// <summary>
        /// Переменная которая хранит время на перезарядку ракет
        /// </summary>
        private double reloadMisslesTime = 10;

        /// <summary>
        /// Переменная которая хранит время на перезарядку бомб
        /// </summary>
        private double reloadBombsTime = 7;

        /// <summary>
        /// Переменная которая хранит количество выпускаемых ракет
        /// </summary>
        private double misslesCount = 5;

        /// <summary>
        /// Переменная которая хранит количество бомб ракет
        /// </summary>
        private double bombsCount = 3;

        /// <summary>
        /// Переменная которая хранит урон ракет
        /// </summary>
        private int missleDamage = 20;

        /// <summary>
        /// Переменная которая хранит скорость ракет
        /// </summary>
        private float missleSpeed = 5f;

        /// <summary>
        /// Переменная которая хранит урон бомб
        /// </summary>
        private int bombDamage = 25;

        /// <summary>
        /// Переменная которая хранит скорость бомб
        /// </summary>
        private float bombSpeed = 3f;

        /// <summary>
        /// Переменная которая хранит здоровье игрока
        /// </summary>
        private double hp = 100;

        /// <summary>
        /// Переменная которая хранит броню игрока
        /// </summary>
        private double armor = 50;

        /// <summary>
        /// Переменная которая хранит топливо игрока
        /// </summary>
        private double fuel = 100;

        /// <summary>
        /// Переменная которая хранит значение нажатия на левую стрелку
        /// </summary>
        private bool left;

        /// <summary>
        /// Переменная которая хранит значение нажатия на правую стрелку
        /// </summary>
        private bool right;

        /// <summary>
        /// Переменная которая хранит значение нажатия на стрелку вниз
        /// </summary>
        private bool down;

        /// <summary>
        /// Переменная которая хранит значение появления ветра влево
        /// </summary>
        private bool windLeft;

        /// <summary>
        /// Переменная которая хранит значение появления ветра вправо
        /// </summary>
        private bool windRight;

        /// <summary>
        /// Переменная которая хранит значение таймера ветра
        /// </summary>
        private float windTimer;

        private Vector2 position;
        private Vector2 size;
        private Vector2 velocity;

        private Texture2D playerSprite;
        private Texture2D missleLeftSprite;
        private Texture2D missleRightSprite;
        private Texture2D bombSprite;

        private List<RectangleF> rects;
        private RectangleF remotePlayerRect;
        private Player remotePlayer;



        private bool isLocal;
        private bool isTest;

        public int MissleDamage { get => missleDamage; set => missleDamage = value; }
        public float MissleSpeed { get => missleSpeed; set => missleSpeed = value; }
        public int BombDamage { get => bombDamage; set => bombDamage = value; }
        public float BombSpeed { get => bombSpeed; set => bombSpeed = value; }

        public double HP { get => hp; set => hp = value; }
        public double Armor { get => armor; set => armor = value; }
        public double Fuel { get => fuel; set => fuel = value; }
        public bool Left { get => left; set => left = value; }
        public bool Right { get => right; set => right = value; }
        public bool Down { get => down; set => down = value; }
        public bool WindLeft { get => windLeft; set => windLeft = value; }
        public bool WindRight { get => windRight; set => windRight = value; }

        public Vector2 Position { get => position; set => position = value; }
        public Vector2 Velocity { get => velocity; set => velocity = value; }

        public RectangleF RemotePlayerRect { get => remotePlayerRect; set => remotePlayerRect = value; }
        public Player RemotePlayer { get => remotePlayer; set => remotePlayer = value; }

        public RectangleF ColRec
        {
            get
            {
                return new RectangleF(position.X - size.X / 2f, position.Y - size.Y / 2f, size.X, size.Y);
            }
        }
        public RectangleF DrawRec
        {
            get
            {
                RectangleF colRec = ColRec;
                colRec.X = colRec.X - 5;
                colRec.Width = colRec.Width + 10;
                return colRec;
            }
        }

        public Texture2D PlayerSprite { get => playerSprite; set => playerSprite = value; }

        public List<Weapon> Missles { get => missles; set => missles = value; }
        public List<Weapon> Bombs { get => bombs; set => bombs = value; }
        public List<RectangleF> Rects { get => rects; set => rects = value; }

        public Player(Vector2 startPos, bool isLocal, bool isTest = false)
        {
            this.position = startPos;
            this.velocity = Vector2.Zero;
            this.size = new Vector2(80, 80);
            this.isLocal = isLocal;
            this.isTest = isTest;
            this.left = false;
            this.right = false;
            this.down = false;
            this.windLeft = false;
            this.windRight = false;
            this.windTimer = 10f;
            rects = new List<RectangleF>();

            missles = new List<Weapon>();
            bombs = new List<Weapon>();
            if (!isTest)
            {
                missleLeftSprite = ContentPipe.LoadTexture("RMissle.png");
                missleRightSprite = ContentPipe.LoadTexture("LMissle.png");
                bombSprite = ContentPipe.LoadTexture("Bomb.png");
            }
            missleSize = new Vector2(20, 10);
            bombSize = new Vector2(20, 30);
        }

        /// <summary>
        /// Метод отвечающий за обработку коллизий, нажатий на клавиши, появления ветра
        /// </summary>
        public void Update()
        {
            HandleInput();
            ResolveCollisions();
            CheckHit();
            if(windLeft || windRight)
                Wind();
        }

        /// <summary>
        /// Метод отвечающий за обработку нажатий на клавиши
        /// </summary>
        private void HandleInput()
        {
            if (isLocal && !isTest)
            {
                if (Input.KeyPress(OpenTK.Input.Key.Left))
                {
                    if (misslesCount > 0)
                    {
                        missles.Add(new Missle(position, missleSize, missleLeftSprite, isLocal, true, missleDamage, missleSpeed));
                        left = true;
                        misslesCount--;
                    }
                    
                }
                else
                    left = false;

                if (Input.KeyPress(OpenTK.Input.Key.Right))
                {
                    if (misslesCount > 0)
                    {
                        missles.Add(new Missle(position, missleSize, missleRightSprite, isLocal, false, missleDamage, missleSpeed));
                        right = true;
                        misslesCount--;
                    }
                }
                else
                    right = false;
                if (Input.KeyPress(OpenTK.Input.Key.Down))
                {
                    if (bombsCount > 0)
                    {
                        bombs.Add(new Bomb(position, bombSize, bombSprite, isLocal, bombDamage, bombSpeed));
                        down = true;
                        bombsCount--;
                    }
                }
                else
                    down = false;
                if (Input.KeyDown(OpenTK.Input.Key.Space) && fuel >= 0)
                {
                    velocity += new Vector2(0, -0.04f);
                    fuel -= 0.05;
                }
                else if (Input.KeyDown(OpenTK.Input.Key.LControl) && fuel >= 0)
                {
                    velocity += new Vector2(0, 0.04f);
                    fuel -= 0.01;
                }
                else
                    velocity += new Vector2(0, 0.005f);

                position += velocity;

                if (misslesCount == 0)
                {
                    reloadMisslesTime -= 0.1f;
                    if (reloadMisslesTime <= 0)
                    {
                        misslesCount = 5;
                        reloadMisslesTime = 10;
                    }
                }
                if (bombsCount == 0)
                {
                    reloadBombsTime -= 0.1f;
                    if (reloadBombsTime <= 0)
                    {
                        bombsCount = 3;
                        reloadBombsTime = 7;
                    }
                }
                Input.Update();
            }
            else
            {
                if (left)
                {
                    missles.Add(new Missle(position, missleSize, missleLeftSprite, isLocal, true, missleDamage, missleSpeed));
                }
                if (right)
                {
                    missles.Add(new Missle(position, missleSize, missleRightSprite, isLocal, false, missleDamage, missleSpeed));
                }
                if (down)
                {
                    bombs.Add(new Bomb(position, bombSize, bombSprite, isLocal, bombDamage, bombSpeed));
                }
            }

        }

        /// <summary>
        /// Метод отвечающий за попадание снарядом в цель
        /// </summary>
        public void CheckHit()
        {
            try
            {
                foreach (var missle in missles)
                {
                    if (missle.Hit || missle.Lifetime <= 0)
                    {
                        missles.Remove(missle);
                    }
                }
                foreach (var bomb in bombs)
                {
                    if (bomb.Hit || bomb.Lifetime <= 0)
                    {
                        bombs.Remove(bomb);
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Метод отвечающий за появление ветра и вычисление позиции игрока реагирующего на ветер
        /// </summary>
        private void Wind()
        {    
            if (windTimer > 0)
            {
                windTimer -= 0.06f;
            }
            if(windTimer > 5)
            {
                if (windLeft)
                    velocity += new Vector2(0.02f, 0);
                if (windRight)
                    velocity += new Vector2(-0.02f, 0);
            }
            if (windTimer < 5)
            {
                if (windLeft)
                    velocity -= new Vector2(0.02f, 0);
                if (windRight)
                    velocity -= new Vector2(-0.02f, 0);
            }
            if (windTimer <= 0)
            {
                velocity = new Vector2(0, velocity.Y);
                windTimer = 10;
                windLeft = false;
                windRight = false;
            }
            if(isTest)
            {
                position += velocity;
            }
        }

        /// <summary>
        /// Метод отвечающий за обновление позиций ракет
        /// </summary>
        public void UpdateMissles()
        {
            foreach(var missle in missles)
            {
                if (remotePlayerRect != null)
                {
                    missle.RemotePlayerRect = remotePlayerRect;
                    missle.RemotePlayer = remotePlayer;
                    missle.Update();
                }
                else
                {
                    missle.Update();
                }
            }
        }

        /// <summary>
        /// Метод отвечающий за обновление позиций бомб
        /// </summary>
        public void UpdateBombs()
        {
            foreach (var bomb in bombs)
            {
                if (remotePlayerRect != null)
                {
                    bomb.RemotePlayerRect = remotePlayerRect;
                    bomb.RemotePlayer = remotePlayer;
                    bomb.Update();
                }
                else
                {
                    bomb.Update();
                }
            }
        }

        /// <summary>
        /// Метод отвечающий за отрисовку ракет
        /// </summary>
        /// <param name="txtLeft"></param>
        /// <param name="txtRight"></param>
        public void DrawMissles(Texture2D txtLeft, Texture2D txtRight)
        {
            foreach (var missle in missles)
            {
                Missle m = (Missle)missle;
                if (!m.Direct)
                    missle.Draw(txtLeft);
                else
                    missle.Draw(txtRight);
            }
        }

        /// <summary>
        /// Метод отвечающий за отрисовку бомб
        /// </summary>
        /// <param name="txt"></param>
        public void DrawBombs(Texture2D txt)
        {
            foreach (var bomb in bombs)
            {
                bomb.Draw(txt);
            }
        }

        /// <summary>
        /// Метод отвечающий за вычисление коллизий
        /// </summary>
        public void ResolveCollisions()
        {
            foreach (var rect in rects)
            {
                RectangleF newRect = ColRec;
                newRect.Height = ColRec.Height + 150;
                if (rect.IntersectsWith(newRect))
                {
                    hp = 0;
                    velocity = Vector2.Zero;
                }
            }
        }

        /// <summary>
        /// Метод отвечающий за отрисовку игрока
        /// </summary>
        public void Draw()
        {
            RectangleF rec = DrawRec;
            rec.X += rec.Width;
            rec.Width = -rec.Width;
            Spritebatch.DrawSprite(playerSprite, rec);
        }
    }
}
