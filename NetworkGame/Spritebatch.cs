﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;

namespace NetworkGame
{
    /// <summary>
    /// Класс отвечающий за отрисовку всех созданых объектов на экране
    /// </summary>
    public class Spritebatch
    {
        /// <summary>
        /// Метод выполняющий преобразование закруженной картинки в графический элемент OpenGL
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="position"></param>
        /// <param name="scale"></param>
        /// <param name="color"></param>
        /// <param name="origin"></param>
        /// <param name="sourceRec"></param>
        public static void DrawSprite(Texture2D texture, Vector2 position, Vector2 scale, Color color, Vector2 origin, RectangleF? sourceRec = null)
        {
            Vector2[] vertices = new Vector2[4]
            {
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(1, 1),
                new Vector2(0, 1),
            };
            GL.BindTexture(TextureTarget.Texture2D, texture.ID);
            GL.Begin(PrimitiveType.Quads);

            for (int i = 0; i < 4; i++)
            {
                if(sourceRec == null)
                    GL.TexCoord2(vertices[i]);
                else
                {
                    GL.TexCoord2(
                        (sourceRec.Value.X + vertices[i].X * sourceRec.Value.Width) / (float)texture.Width,
                        (sourceRec.Value.Y + vertices[i].Y * sourceRec.Value.Height) / (float)texture.Height);
                }
                vertices[i].X *= texture.Width;
                vertices[i].Y *= texture.Height;
                vertices[i] -= origin;
                vertices[i] *= scale;
                vertices[i] += position;

                GL.Vertex2(vertices[i]);
            }
            GL.End();
        }
        public static void Begin(int screenWidth, int screenHeight)
        {
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(-screenWidth / 2f, screenWidth / 2f, screenHeight / 2f, -screenHeight / 2, 0f, 1f);
        }
        
        public static void DrawSprite(Texture2D texture, RectangleF rectangle, Color color, RectangleF? sourceRec = null)
        {
            DrawSprite(texture, new Vector2(rectangle.X, rectangle.Y), new Vector2(rectangle.Width / texture.Width, rectangle.Height / texture.Height), color, Vector2.Zero, sourceRec);
        }
        public static void DrawSprite(Texture2D texture, RectangleF rectangle)
        {
            DrawSprite(texture, new Vector2(rectangle.X, rectangle.Y), new Vector2(rectangle.Width / texture.Width, rectangle.Height / texture.Height), Color.White, Vector2.Zero);
        }    
    }
}
