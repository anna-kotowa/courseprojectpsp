﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetworkGame;
using NetworkGame.Weapons;
using OpenTK;

namespace HotAirballoonGameTest
{
    [TestClass]
    public class MoveMissleAndBombTest
    {
        Missle missle = new Missle(new Vector2(0f, 0f), new Vector2(20f, 20f), new Texture2D(), true, true, 10, 10);
        Bomb bomb = new Bomb(new Vector2(0f, 0f), new Vector2(20f, 20f), new Texture2D(), true, 10, 10);

        [TestMethod]
        public void MoveMissleTest()
        {
            Vector2 positionAssert = new Vector2(-100f, 0);
            for (int i = 0; i < 10; i++)
            {
                missle.Update();
            }
            bool result = positionAssert.X == missle.Position.X && positionAssert.Y == missle.Position.Y ? true : false;
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void MoveBombTest()
        {
            Vector2 positionAssert = new Vector2(0, 100f);
            for (int i = 0; i < 10; i++)
            {
                bomb.Update();
            }
            bool result = positionAssert.X == bomb.Position.X && positionAssert.Y == bomb.Position.Y ? true : false;
            Assert.IsTrue(result);
        }
    }
}
