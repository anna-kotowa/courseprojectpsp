﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetworkGame;
using NetworkGame.Weapons;
using OpenTK;

namespace HotAirballoonGameTest
{
    [TestClass]
    public class PlayerTest
    {
        Player localPlayer = new Player(new Vector2(0f, 0f), true, true);

        [TestMethod]
        public void MovePlayerTest()
        {
            Vector2 positionAssert = new Vector2(0, 100f);
            for (int i = 0; i < 10; i++)
            {
                localPlayer.Position += new Vector2(0, 10f);
            }
            bool result = positionAssert.X == localPlayer.Position.X && positionAssert.Y == localPlayer.Position.Y ? true : false;
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void MoveWindLeftPlayerTest()
        {
            int posX = 1;
            localPlayer.Position = Vector2.Zero;
            localPlayer.WindRight = false;
            localPlayer.WindLeft = true;
            
            for (int i = 0; i < 10; i++)
            {
                localPlayer.Update();
            }
            Assert.AreEqual(posX, Convert.ToInt32(localPlayer.Position.X));
        }

        [TestMethod]
        public void MoveWindRightPlayerTest()
        {
            int posX = -1;
            localPlayer.Position = Vector2.Zero;
            localPlayer.WindRight = true;
            localPlayer.WindLeft = false;
            for (int i = 0; i < 10; i++)
            {
                localPlayer.Update();
            }
            Assert.AreEqual(posX, Convert.ToInt32(localPlayer.Position.X));
        }

        [TestMethod]
        public void HpPlayerNullTest()
        {
            int hp = 0;
            for (int i = 0; i < 10; i++)
            {
                localPlayer.HP -= 10;
            }
            Assert.AreEqual(hp, localPlayer.HP);
        }

    }
}
