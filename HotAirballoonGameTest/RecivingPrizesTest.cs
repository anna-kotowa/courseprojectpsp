﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetworkGame;
using NetworkGame.Weapons;
using OpenTK;

namespace HotAirballoonGameTest
{
    [TestClass]
    public class RecivingPrizesTest
    {
        HPPrize hpPrize;
        ArmorPrize armorPrize;
        FuelPrize fuelPrize;
        DamagePrize damagePrize;
        SpeedPrize speedPrize;
        Player localPlayer = new Player(new Vector2(40f, 0f), true, true);
        Player remotePlayer = new Player(new Vector2(100f, 0f), false, true);
        Missle missle = new Missle(new Vector2(40f, 0f), new Vector2(20f, 20f), new Texture2D(), true, true, 10, 10);

        [TestMethod]
        public void HPPrizeGetTest()
        {
            localPlayer.HP = 50;
            hpPrize = new HPPrize(Vector2.Zero, Vector2.Zero, localPlayer, remotePlayer, true);
            Vector2 positionAssert = new Vector2(40f, 0);
            
            for (int i = 0; i < 10; i++)
            {
                hpPrize.Update();
            }
            
            int hp = 75;
            Assert.AreEqual(hp, localPlayer.HP);
        }

        [TestMethod]
        public void ArmorPrizeGetTest()
        {
            localPlayer.Armor = 10;
            armorPrize = new ArmorPrize(Vector2.Zero, Vector2.Zero, localPlayer, remotePlayer, true);
            Vector2 positionAssert = new Vector2(40f, 0);
            for (int i = 0; i < 10; i++)
            {
                armorPrize.Update();
            }
            int armor = 50;
            Assert.AreEqual(armor, localPlayer.Armor);
        }

        [TestMethod]
        public void FuelPrizeGetTest()
        {
            localPlayer.Fuel = 10;
            fuelPrize = new FuelPrize(Vector2.Zero, Vector2.Zero, localPlayer, remotePlayer, true);
            Vector2 positionAssert = new Vector2(40f, 0);
            for (int i = 0; i < 10; i++)
            {
                fuelPrize.Update();
            }
            double fuel = 100;
            Assert.AreEqual(fuel, localPlayer.Fuel);
        }

        [TestMethod]
        public void DamagePrizeGetTest()
        {
            localPlayer.Missles.Add(missle);
            damagePrize = new DamagePrize(Vector2.Zero, Vector2.Zero, localPlayer, remotePlayer, true);
            Vector2 positionAssert = new Vector2(40f, 0);
            for (int i = 0; i < 10; i++)
            {
                damagePrize.Update();
            }
            int damage = 15;
            Assert.AreEqual(damage, missle.Damage);
        }

        [TestMethod]
        public void SpeedPrizeGetTest()
        {
            localPlayer.Missles.Add(missle);
            speedPrize = new SpeedPrize(Vector2.Zero, Vector2.Zero, localPlayer, remotePlayer, true);
            Vector2 positionAssert = new Vector2(40f, 0);
            for (int i = 0; i < 10; i++)
            {
                speedPrize.Update();
            }
            int speed = 15;
            Assert.AreEqual(speed, missle.Speed);
        }
    }
}
