﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetworkGame;
using NetworkGame.Weapons;
using OpenTK;

namespace HotAirballoonGameTest
{
    [TestClass]
    public class GetHitTest
    {
        Player localPlayer = new Player(new Vector2(40f, 0f), true, true);
        Player remotePlayer = new Player(new Vector2(40f, 0f), false, true);
        Missle missle = new Missle(new Vector2(40f, 0f), new Vector2(20f, 20f), new Texture2D(), true, true, 10, 10);
        Bomb bomb = new Bomb(new Vector2(40f, 0f), new Vector2(20f, 20f), new Texture2D(), true, 10, 10);

        [TestMethod]
        public void GetHitMissleTest()
        {
            missle.RemotePlayer = remotePlayer;
            missle.RemotePlayerRect = remotePlayer.ColRec;
            Vector2 positionAssert = new Vector2(40f, 0);
            missle.ResolveCollision();
            int hp = 90;
            Assert.AreEqual(hp, remotePlayer.HP);
        }

        [TestMethod]
        public void GetHitBombTest()
        {
            bomb.RemotePlayer = remotePlayer;
            bomb.RemotePlayerRect = remotePlayer.ColRec;
            Vector2 positionAssert = new Vector2(40f, 0);
            bomb.ResolveCollision();
            int hp = 90;
            Assert.AreEqual(hp, remotePlayer.HP);
        }
    }
}
