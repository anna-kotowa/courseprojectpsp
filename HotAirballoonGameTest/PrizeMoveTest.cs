﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetworkGame;
using OpenTK;

namespace HotAirballoonGameTest
{
    [TestClass]
    public class PrizeMoveTest
    {
        HPPrize hpPrize = new HPPrize(Vector2.Zero, Vector2.Zero, null, null, true);
        ArmorPrize armorPrize = new ArmorPrize(Vector2.Zero, Vector2.Zero, null, null, true);
        FuelPrize fuelPrize = new FuelPrize(Vector2.Zero, Vector2.Zero, null, null, true);
        DamagePrize damagePrize = new DamagePrize(Vector2.Zero, Vector2.Zero, null, null, true);
        SpeedPrize speedPrize = new SpeedPrize(Vector2.Zero, Vector2.Zero, null, null, true);

        [TestMethod]
        public void HPPrizeMoveTest()
        {
            Vector2 positionAssert = new Vector2(40f, 0);
            for(int i = 0; i < 10; i++)
            {
                hpPrize.Update();
            }
            bool result = positionAssert.X == hpPrize.Position.X && positionAssert.Y == hpPrize.Position.Y ? true : false;
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ArmorPrizeMoveTest()
        {
            Vector2 positionAssert = new Vector2(40f, 0);
            for (int i = 0; i < 10; i++)
            {
                armorPrize.Update();
            }
            bool result = positionAssert.X == armorPrize.Position.X && positionAssert.Y == armorPrize.Position.Y ? true : false;
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void FuelPrizeMoveTest()
        {
            Vector2 positionAssert = new Vector2(40f, 0);
            for (int i = 0; i < 10; i++)
            {
                fuelPrize.Update();
            }
            bool result = positionAssert.X == fuelPrize.Position.X && positionAssert.Y == fuelPrize.Position.Y ? true : false;
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DamagePrizeMoveTest()
        {
            Vector2 positionAssert = new Vector2(40f, 0);
            for (int i = 0; i < 10; i++)
            {
                damagePrize.Update();
            }
            bool result = positionAssert.X == damagePrize.Position.X && positionAssert.Y == damagePrize.Position.Y ? true : false;
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void SpeedPrizeMoveTest()
        {
            Vector2 positionAssert = new Vector2(40f, 0);
            for (int i = 0; i < 10; i++)
            {
                speedPrize.Update();
            }
            bool result = positionAssert.X == speedPrize.Position.X && positionAssert.Y == speedPrize.Position.Y ? true : false;
            Assert.IsTrue(result);
        }
    }
}
